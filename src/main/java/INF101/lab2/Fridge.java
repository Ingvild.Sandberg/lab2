package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	//list of items in the fridge 
	List<FridgeItem> items = new ArrayList<>(); 
	
	// Amount of fridgeItems possible in fridge  
	int maxCap = 20; 
	
	/**
	 * Returns the number of items currently in the fridge 
	 * 
	 * @return number of items in the fridge 
	 */
	public int nItemsInFridge() {
		int size = items.size(); 
		return size; 		
	}
	
	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	public int totalSize() {
		return maxCap; 
	}
	
	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < 20) {
			items.add(item);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	public void takeOut(FridgeItem item) {
		if (items.contains(item)) {
			items.remove(item); 
		} else {
			throw new NoSuchElementException();
		}
	}
	
	/**
	 * Remove all items from the fridge
	 */
	public void emptyFridge() {
		items.clear();
	}
	
	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	public List<FridgeItem> removeExpiredFood(){ 
		List<FridgeItem> expiredFood = new ArrayList<>();
		int nrItems = nItemsInFridge();
		int index = 0;
		
		for (int i = 0; i < nrItems; i++) {	
			if (items.get(index).hasExpired()) {
				expiredFood.add(items.get(index));
				takeOut(items.get(index));
			} else {
				index++;
			}
		}
		return expiredFood;
	}
		

}
